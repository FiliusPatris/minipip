# minipip - Small Docker-based CI/CD pipelines

This small program was designed, because I was frustrated by the complexity of Jenkins et al. When Docker already provides
isolation and a build recepie, why are we in the need of other build recepies and a complex setup of Jenkins or other
pipelines?

Also I wanted to implement usable CI/CD pipelines compatible with [codeberg](https://codeberg.org/)'s Gitea webhooks.

## Usage

On your server:

 - Make sure you have [Docker](https://www.docker.com/) installed and the API version is
   v1.18 or higher (`docker version` > Server > Engine > API version). Also make sure you have [docker-compose](https://docs.docker.com/compose/) installed.
 - Get the `docker-compose.yaml` file to your server.
 - Write the .env File (See 'Cofiguration' section for more infos).
 - Spin the container up: `docker-compose up -d`.

In your repo:
 - Place the Dockerfile for deployment into `.minipip/Dockerfile`.
 - Register a webhook on push events and point them to `https://<your-server>/trigger`.
   Make sure to set the same (sufficiently long) secret as in the `.env` file.
 - push to the branch you want the docker image to be tagged with.

### Production
Be sure to put it behind a reverse proxy with SSL when using in production.
Otherwhise someone might be able to sniff the secret and run unauthorized builds.
This should not lead to full RCE on the host, but I take no guarantee.

## Configuration
Configuration happens via the `.env` file, which will be read by `docker-compose`.

These are the available variables:

 - `REG_UNAME`: The username for the login at the registry specified in `REG_SERVER`.
 - `REG_PASSWD`: The password for the login at the registry specified in `REG_SERVER`.
 - `REG_EMAIL`: The email for the login at the registry specified in `REG_SERVER`.
 - `REG_SERVER`: The address of the Docker registry server.
 - `JOBS`: A JSON string containing the availible jobs. Only jobs specified in this object can be run.
 For this repo it might look something like this (more info in the `.env.template` file):
```js
{
  "FiliusPatris/minipip": {
    "secret": "ABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890",
    "imgName": "filiuspatris/minipip"
   },
   // ... more jobs
}
 ```

## Future
In the near future (when my project grows to that state) I will implement more pipelines(eg, continous testing,
continuous deployment). Then you'll be able to spcify certain actions to run when pushing to a certain branch.

## References

- [Docker socket API](https://docs.docker.com/engine/api/v1.40/)
- [Gitea webhooks reference](https://docs.gitea.io/en-us/webhooks/)
- [GitHub webhooks reference (push)](https://developer.github.com/webhooks/event-payloads/#push)