// Application local config
require('dotenv').config();

// Parse env variables
var config = {};
{
	// Parse and attach auth token
	regDetails = {
		"username": process.env.REG_UNAME,
		"password": process.env.REG_PASSWD,
		"email": process.env.REG_EMAIL,
		"serveraddress": process.env.REG_SERVER
	}
	config.authToken = Buffer.from(JSON.stringify(regDetails)).toString('base64');

	// Parse and attach job configs
	config.jobs = JSON.parse(process.env.JOBS);
}

// Express setup
const express = require('express');
const app = express();
app.use(express.json());
const morgan = require('morgan');
app.use(morgan('short'));

// Auth & sanitization middleware
app.use((req, res, next) => {
	try {
		const jobConfig = config.jobs[req.body.repository.full_name];
		if (typeof jobConfg === undefined) {
			// No config for this job
			res.sendStatus(404);
			return;
		} else if (req.body.secret != jobConfig.secret) {
			// There is a config, but another secret
			res.sendStatus(401);
			return;
		} else {
			// OK: Attach this particular config to req.
			req.config = jobConfig;
			next();
			res.sendStatus(200);
		}
	} catch (e) {
		console.log(e);
		// Maybe not all the correct body parameters
		res.sendStatus(400);
		return;
	}
});

// And finally process the job pipeline
var pipeline = require('./pipeline');

app.post('/:job', (req) => {
	// Full image name consists of the imgName specified in JOBS envvar
	// and a tag that is has the branch name
	const branch = req.body.ref.split('/').slice('-1').pop();
	const img_tag = req.config.imgName + ':' + branch;
	// The url has #commit attached, to immediately fetch the requested commit
	// WARNING: Many servers do not allow fetching unadvertised commits.
	//          This is not a problem if the commit is pointed to by a branch,
	//          and thus most times works.
	const commit = req.body.after;
	const repo_url = req.body.repository.clone_url + '#' + commit;
	// A unique job id. Consisitng of date/time and the commit.
	const job_id = new Date(Date.now()).toISOString() + '__' + req.body.after;

	// Call the async function; Don't await the response.
	pipeline(img_tag, repo_url, config.authToken, job_id);
});


// Listen
app.listen(80);