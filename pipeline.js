const { encode } = require('url-encode-decode');
const exec = require('util').promisify(require('child_process').exec);
const fs = require('fs');

async function build(repo_url, img_name, logfile) {
	const build_api_url = `"http:/v1.40/build?remote=${encode(repo_url)}&t=${encode(img_name)}&dockerfile=.minipip%2FDockerfile"`;
	const build_curl_cmd = `curl -s --unix-socket /var/run/docker.sock -X POST ${build_api_url};\n`;

	const { stdout, stderr } = await exec(build_curl_cmd);
	if (stderr) {
		console.warn('cURL error: ' + stderr);
		fs.appendFile(logfile, build_curl_cmd + buildstdout + '\n Error:\n' + stderr,
			err => { if (err) throw err });
	}
	if (stdout) {
		console.log(`Built image ${img_name}.`);
		fs.appendFile(logfile, build_curl_cmd + stdout,
			err => { if (err) throw err });
	}
}

async function push(img_name, auth_token, logfile) {
	const push_api_url = `"http:/v1.40/images/${encode(img_name)}/push"`;
	const auth_header = `-H "X-Registry-Auth: ${auth_token}"`;
	const push_curl_cmd = `curl -s --unix-socket /var/run/docker.sock ${auth_header} -X POST ${push_api_url};\n`;

	const { stdout, stderr } = await exec(push_curl_cmd);
	if (stderr) {
		console.warn('cURL error: ' + stderr);
		fs.appendFile(logfile, push_curl_cmd + stdout + '\n Error:\n' + stderr,
			err => { if (err) throw err });
	}
	if (stdout) {
		console.log(`Pushed image ${img_name}.`);
		fs.appendFile(logfile, push_curl_cmd + stdout,
			err => { if (err) throw err });
	}
}

module.exports = async (img_name, repo_url, auth_token, job_id) => {
	const logfile = process.env.LOG_DIR + '/' + job_id + '.log';
	await build(repo_url, img_name, logfile);
	await push(img_name, auth_token, logfile);
}